#include <BigFontLCD.h> /* https://bitbucket.org/grafviktor/bigfontlcd */
#include <RTC.h>
#include <DHT.h>
#include <LiquidCrystal_I2C.h>

const byte DHTPIN = 2; // DHT11 pin
const byte CLK = 5;
const byte DAT = 4;
const byte RST = 3;

DHT dht(DHTPIN, DHT11);
LiquidCrystal_I2C lcd(0x27, 16, 2);
RTC time;
BigFontLCD *bfl = 0;
//BigFontLCD bfl(&lcd);

boolean swtch = true;

void setup() {
  //  Serial.begin(9600);
  lcd.init();
  lcd.backlight();
  bfl = new BigFontLCD(&lcd);
  dht.begin();

  //time
  time.begin(RTC_DS1302, RST, CLK, DAT);
  /* uncomment if time adjustment is needed
     FORMAT: time.settime(SEC,MIN,HOUR,DAY,MONTH,YEAR, DAY_OF_WEEK);
  */
  // time.settime(00,01,18,28,11,15,6);
}

void loop() {
  if (swtch) {
    displayCurrentTime();
  } else {
    displayCurrentConditions();
  }
  swtch = !swtch ;
  //  Serial.print("Counter: ");
  //  Serial.println(counter);
  lcd.clear(); // takes 2000
}

void displayCurrentTime() {
  char buf[9];
  strcpy(buf, time.gettime("H:i"));
  formatTime(buf);
  for(byte i=0; i<21; ++i) {
    bfl->print(buf, 0, 0);    
    delay(1000);
    blinkColon(buf);
  }  
  //  bfl.print(buf, 0, 0);
  //  Serial.print("Time: ");
  //  Serial.println(fmt_time);
  //  delay(16000);
}

void blinkColon(char *s) {
  s[4] == ':' ? s[4] = ' ' : s[4] = ':';
}

void formatTime(char* s) {
  s[8] = '\0';
  s[7] = s[4];
  s[6] = ' ';
  s[5] = s[3];
  s[4] = ':';
  s[3] = ' ';
  s[2] = s[1];
  s[1] = ' ';
  // s[0] = s[0]; - nothing to change!
}

void displayCurrentConditions() {
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  byte t = dht.readTemperature();
  byte h = dht.readHumidity();

  // check if returns are valid, if they are NaN (not a number) then something went wrong!
  if (isnan(t) || isnan(h)) {
    lcd.print("Failed to read from DHT");
  } else {
    displayCondition("T: ", t, "C");
    displayCondition("H: ", h, "%");
  }
}

void displayCondition(char *prefix, byte val, char *postfix) {
  //  val = -val;
  byte postfix_position = 11;
  if (val < 0)
    postfix_position = 14;

  char str_val[4];
  itoa(val, str_val, 10);

  char condition[8];
  strcpy(condition, prefix); //+ humidity;
  strcat(condition, str_val);
  bfl->print(condition, 0 , 0);
  lcd.setCursor(postfix_position, 0);
  lcd.print(postfix);
  //  Serial.print("Condition: ");
  //  Serial.println(condition);
  delay(2000);
}

